package springminiproject.com.somnakspringminiproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springminiproject.com.somnakspringminiproject.model.Book;
import springminiproject.com.somnakspringminiproject.model.Category;
import springminiproject.com.somnakspringminiproject.services.BookService;
import springminiproject.com.somnakspringminiproject.services.CategoryService;
import springminiproject.com.somnakspringminiproject.services.UploadService;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
public class BookController {
    private BookService bookService;

    @Autowired
    private CategoryService categoryService;

    private UploadService uploadService;
    @Autowired
    public BookController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }
    @GetMapping({"/home","/index","/","/dashboard"})
    public String allBook(ModelMap model) {
        List<Category> listcate=this.categoryService.getAll();
        Integer catecount=listcate.size();
        Integer bookcount=this.bookService.getCount();
        List<Book> latestbooks=this.bookService.getLatestBooks();
        model.addAttribute("books", latestbooks);
        model.addAttribute("cate_count",catecount);
        model.addAttribute("book_count",bookcount);
        return "admin/dashboard";
    }
    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id") Integer id, Model model) {
        System.out.println(id);
        Book book = this.bookService.searchBook(id);
        System.out.println(book);
        model.addAttribute("book", book);
        return "admin/detail";
    }
    @GetMapping("/booklist")
    public String showallbook(Model model){
        List<Book> books=this.bookService.getBooks();
        model.addAttribute("books",books);
        return "admin/booklist";
    }

    @GetMapping("/update/{id}")
    public String showUpdateForm(@PathVariable Integer id, ModelMap modelMap) {

        Book book = this.bookService.searchBook(id);

        modelMap.addAttribute("createNew", false);
        modelMap.addAttribute("book", book);

        List<Category> categories = this.categoryService.getAll();
        modelMap.addAttribute("categories", categories);


        return "admin/bookform";
    }


    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file) {
        File path = new File("/images-somnak");

        if (!path.exists())
            path.mkdir();


        String filename = file.getOriginalFilename();

        String extension = filename.substring(filename.lastIndexOf('.') + 1);

        filename = UUID.randomUUID() + "." + extension;
        try {
            Files.copy(file.getInputStream(), Paths.get("/images-somnak/", filename));
        } catch (IOException e) {
        }
        System.out.println(filename);
        if (!file.isEmpty()) {
            book.setThumbnail("/images-somnak/" + filename);
        }
        System.out.println(book);
        boolean ch=this.bookService.updateBook(book);
        System.out.println(ch);
        return "redirect:/booklist";
    }


    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.bookService.deleteBook(id);
        return "redirect:/booklist";
    }


    @GetMapping("/count")
    @ResponseBody
    public Map<String, Object> count() {
        Map<String, Object> response = new HashMap<>();

        response.put("record_count", this.bookService.getCount());
        response.put("status", true);

        return response;
    }


    @GetMapping("/create")
    public String create(Model model) {

        model.addAttribute("book", new Book());
        model.addAttribute("createNew", true);

        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("categories", categories);

        return "admin/bookform";
    }


    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file, Model model) {
        System.out.println(book);

        if (bindingResult.hasErrors()) {
            model.addAttribute("createNew", true);
            return "admin/bookform";
        }

        String filename = this.uploadService.upload(file);
        book.setThumbnail("/images-somnak/" + filename);

        this.bookService.addBook(book);

        return "redirect:/home";
    }

}
