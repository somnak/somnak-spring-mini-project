package springminiproject.com.somnakspringminiproject.repositories.providers;

import org.apache.ibatis.jdbc.SQL;
import springminiproject.com.somnakspringminiproject.model.Book;

public class BookProvider {

        public String getAllProvider(){
            return new SQL(){{
                SELECT("*");
                FROM("tb_book b");
                INNER_JOIN("tb_category c ON b.cate_id = c.id");
                ORDER_BY("b.id desc");
            }}.toString();
        }
        public String getLatestBook(){
            return new SQL(){{
                SELECT("*");
                FROM("tb_book b");
                INNER_JOIN("tb_category c ON b.cate_id = c.id");
                ORDER_BY("b.id desc LIMIT 10");
            }}.toString();
        }

        public String create(Book book){
            return new SQL(){{
                INSERT_INTO("tb_book");
                VALUES("title", "#{title}");
                VALUES("author", "#{author}");
                VALUES("publisher", "#{publisher}");
                VALUES("publishdate","#{publishdate}");
                VALUES("description", "#{description}");
                VALUES("thumbnail", "#{thumbnail}");
                VALUES("cate_id" ,"#{category.id}");

            }}.toString();
        }
//        public String searchBook(Integer id){
//            return new SQL(){{
//                SELECT("*");
//                FROM("tb_book b");
//                INNER_JOIN("tb_category c ON b.cate_id = c.id");
//                WHERE("b.id=#{id}");
//            }}.toString();
//        }



}
