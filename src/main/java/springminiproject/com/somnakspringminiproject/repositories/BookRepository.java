package springminiproject.com.somnakspringminiproject.repositories;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import springminiproject.com.somnakspringminiproject.model.Book;
import springminiproject.com.somnakspringminiproject.repositories.providers.BookProvider;

import java.util.List;

@Repository
public interface BookRepository {
    @SelectProvider(type = BookProvider.class, method = "getAllProvider")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> getAll();
//    @Select("SELECT * FROM tb_book b JOIN tb_category c on c.id=b.cate_id ORDER BY b.id DESC LIMIT #{l} OFFSET #{offset}")
//    List<Book> getAll(@Param("l") Integer l,@Param("offset") Integer offset);

    @Select("select * from tb_book b INNER JOIN tb_category c ON b.cate_id = c.id where b.id=#{id}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    Book searchBook(@Param("id") Integer id);

    @SelectProvider(type = BookProvider.class, method = "getLatestBook")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> getLatestBook();
    @Update("update tb_book set title=#{title}, author=#{author}, publisher=#{publisher},publishdate=#{publishdate},description=#{description}, thumbnail=#{thumbnail}, cate_id=#{category.id} where id=#{id}")
    boolean updateBook(Book book);


    @Delete("delete from tb_book where id=#{id}")
    boolean deleteBook(@Param("id") Integer id);


    @Select("select count(*) from tb_book")
    Integer getCount();


    @InsertProvider(type = BookProvider.class, method = "create")
    boolean addBook(Book book);
}