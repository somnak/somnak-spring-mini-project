package springminiproject.com.somnakspringminiproject.model;

import java.util.List;

public class Category {
    private int id;
    private String name;
    List<Book> books;
    public Category() {
    }

    public Category(int id, String cate_name, List<Book> listbooks) {
        this.id = id;
        this.name = name;
        this.books = books;
    }

    public List<Book> getListbooks() {
        return books;
    }

    public void setListbooks(List<Book> listbooks) {
        this.books = listbooks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCate_name() {
        return name;
    }

    public void setCate_name(String cate_name) {
        this.name = cate_name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", cate_name='" + name + '\'' +
                ", listbooks=" + books +
                '}';
    }
}
