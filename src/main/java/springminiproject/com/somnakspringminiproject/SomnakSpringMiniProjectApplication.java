package springminiproject.com.somnakspringminiproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SomnakSpringMiniProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomnakSpringMiniProjectApplication.class, args);
	}
}
