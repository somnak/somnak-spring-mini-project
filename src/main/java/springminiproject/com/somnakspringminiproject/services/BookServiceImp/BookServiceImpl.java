package springminiproject.com.somnakspringminiproject.services.BookServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springminiproject.com.somnakspringminiproject.model.Book;
import springminiproject.com.somnakspringminiproject.repositories.BookRepository;
import springminiproject.com.somnakspringminiproject.services.BookService;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

//    @Override
//    public List<Book> getBooks(int limit, int offset) {
//        return this.bookRepository.getAll(limit,offset );
//    }

    @Override
    public List<Book> getBooks() {
        return this.bookRepository.getAll();
    }

    @Override
    public boolean updateBook(Book book) {
        return this.bookRepository.updateBook(book);
    }

    @Override
    public List<Book> getLatestBooks() {
        return this.bookRepository.getLatestBook();
    }

    @Override
    public boolean deleteBook(int id) {
        return this.bookRepository.deleteBook(id);
    }

    @Override
    public boolean addBook(Book book) {
        return this.bookRepository.addBook(book);
    }

    @Override
    public Book searchBook(int id) {
        return this.bookRepository.searchBook(id);
    }

    @Override
    public Integer getCount() {
        return this.bookRepository.getCount();
    }
}
