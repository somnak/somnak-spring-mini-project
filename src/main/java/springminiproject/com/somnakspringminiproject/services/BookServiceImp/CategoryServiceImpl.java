package springminiproject.com.somnakspringminiproject.services.BookServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springminiproject.com.somnakspringminiproject.model.Category;
import springminiproject.com.somnakspringminiproject.repositories.CategoryRepository;
import springminiproject.com.somnakspringminiproject.services.CategoryService;

import java.util.List;


@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.getAll();
    }

    @Override
    public Integer count() {
        return this.categoryRepository.count();
    }
}
