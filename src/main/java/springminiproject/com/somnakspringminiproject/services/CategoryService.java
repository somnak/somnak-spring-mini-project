package springminiproject.com.somnakspringminiproject.services;
import springminiproject.com.somnakspringminiproject.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();
    Integer count();
}
